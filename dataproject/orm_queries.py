from dataproject.models import Matches, Deliveries
from django.db.models import Count, Avg, Sum


def number_of_matches_played_per_season():
    number_of_matches = Matches.objects.order_by('season').values('season').annotate(
        matches_count=Count('season'))
    return number_of_matches


def extra_runs_conceded_per_team():
    matches_id = Matches.objects.filter(season=2016)
    print(matches_id)
    extra_runs_conceded_per_team = Deliveries.objects.filter(
        match_id__in=matches_id).only("bowling_team").values("bowling_team").annotate(runs=Sum("extra_runs"))
    print(extra_runs_conceded_per_team)
    return extra_runs_conceded_per_team


def economical_bowlers():
    match_id = Matches.objects.filter(season=2015)
    print(match_id)
    bowlers_economy = Deliveries.objects.filter(match_id__in=match_id).values('bowler').annotate(
        avg=Avg('total_runs') * 6).order_by('avg')[:5]
    print(bowlers_economy)
    return bowlers_economy


def get_top_wicket_taker():
    top_wicket_taker = Deliveries.objects.exclude(dismissal_kind='').values("bowler").annotate(
        wicket=Count('dismissal_kind')).order_by('-wicket')[:5]
    return top_wicket_taker


def match_won_per_team_per_season():
    match_won_per_team_per_season = Matches.objects.exclude(winner__isnull=True).exclude(winner__exact='').values(
        "winner", "season").annotate(win=Count('winner'))
    team_record = {}
    seasons = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]
    for winning_team_per_season in match_won_per_team_per_season:
        if winning_team_per_season['winner'] in team_record:
            team_record[winning_team_per_season['winner']][seasons.index(winning_team_per_season['season'])] = \
            winning_team_per_season['win']
        else:
            team_record[winning_team_per_season['winner']] = [0] * 10
            team_record[winning_team_per_season['winner']][seasons.index(winning_team_per_season['season'])] = \
                winning_team_per_season['win']
    return team_record,seasons
