#!/usr/bin/python3
#from django.core.management.base import BaseCommand

#from dataproject.models import Matches, Deliveries
import csv
#from django.core.management.base import BaseCommand
import pymysql

from django.core.management.base import BaseCommand
import pymysql



class Command(BaseCommand):
    help = 'Loads the csv file into mysql database'

    def handle(self, *args, **kwargs):
        mydb = pymysql.connect(
            host="localhost",
            user="root",
           # user="gurs",
            passwd="password",
            database="sql_django",
            autocommit=True,
            local_infile=1
        )
        mycursor = mydb.cursor()

        query_to_transfer_matches_to_mysql = "LOAD DATA LOCAL INFILE './matches.csv' INTO TABLE sql_django.dataproject_matches FIELDS TERMINATED BY ','  IGNORE 1 LINES;"
        mycursor.execute(query_to_transfer_matches_to_mysql)

        query_to_transfer_deliveries_to_mysql = "LOAD DATA LOCAL INFILE './deliveries.csv' INTO TABLE sql_django.dataproject_deliveries FIELDS TERMINATED BY ','  IGNORE 1 LINES(match_id,inning,batting_team,bowling_team,over,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder);"
        mycursor.execute(query_to_transfer_deliveries_to_mysql)

        mycursor.close()
        mydb.close()


# def csv_to_database():
#     mydb = pymysql.connect(
#         host="localhost",
#         user="root",
#         # user="gurs",
#         passwd="password",
#         database="sql_django",
#         autocommit=True,
#         local_infile=1
#     )
#     mycursor = mydb.cursor()
#
#     query_to_transfer_matches_to_mysql = "LOAD DATA LOCAL INFILE './matches.csv' INTO TABLE sql_django.dataproject_matches FIELDS TERMINATED BY ','  IGNORE 1 LINES;"
#     mycursor.execute(query_to_transfer_matches_to_mysql)
#
#     query_to_transfer_deliveries_to_mysql = "LOAD DATA LOCAL INFILE './deliveries.csv' INTO TABLE sql_django.dataproject_deliveries FIELDS TERMINATED BY ','  IGNORE 1 LINES(match_id,inning,batting_team,bowling_team,over,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder);"
#     mycursor.execute(query_to_transfer_deliveries_to_mysql)
#
#     mycursor.close()
#     mydb.close()
#
#
# csv_to_database()
