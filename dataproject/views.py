# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from dataproject.orm_queries import number_of_matches_played_per_season, extra_runs_conceded_per_team, match_won_per_team_per_season, economical_bowlers, get_top_wicket_taker
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

# Create your views here.

@cache_page(CACHE_TTL)
def detail(request):
    return HttpResponse("You are looking for data project")

@cache_page(CACHE_TTL)
def data1(request):
    matches_played_per_season = number_of_matches_played_per_season()
    context = {'matches_played_per_season': matches_played_per_season}
    return render(request, './data1.html', context)

@cache_page(CACHE_TTL)
def data2(request):
    match_won_per_teams_per_season = match_won_per_team_per_season()
    context = {'match_won_per_teams_per_season': match_won_per_teams_per_season[0], 'seasons':match_won_per_teams_per_season[1]}
    return render(request, './data2.html',context)

@cache_page(CACHE_TTL)
def data3(request):
    extra_runs_conceded_by_team = extra_runs_conceded_per_team()
    context = {'extra_runs_conceded_by_team': extra_runs_conceded_by_team}
    return render(request, './data3.html', context)

@cache_page(CACHE_TTL)
def data4(request):
    sorted_economical_bowlers = economical_bowlers()
    context = {'sorted_economical_bowlers': sorted_economical_bowlers}
    return render(request, './data4.html', context)

@cache_page(CACHE_TTL)
def data5(request):
    most_wicket_taker_bowlers = list(get_top_wicket_taker())
    context = {'most_wicket_taker_bowlers': most_wicket_taker_bowlers}
    return render(request, './data5.html', context)
