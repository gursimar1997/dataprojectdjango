from django.urls import path

from . import views

urlpatterns = [
    path('home/', views.detail, name='detail'),
    path('data1/', views.data1, name='data1'),
    path('data2/', views.data2, name='data2'),
    path('data3/', views.data3, name='data3'),
    path('data4/', views.data4, name='data4'),
    path('data5/', views.data5, name='data5'),
]
